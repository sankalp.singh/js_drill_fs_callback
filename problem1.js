/*
    Problem 1:
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/
const fs = require("fs");
const path = require("path");
function problem1(dirName, fileCount, cb) {
	const dirPath = path.join(__dirname, dirName);
	let errLog = [];
	let fileCreatedPaths = [];
	let deleteCount = 0;
	let fsRun = 0;
	fs.mkdir(dirPath, (err) => {
		if (err) {
			cb(Error("Error while making directory"));
		} else {
			for (let fileCreated = 0; fileCreated < fileCount; fileCreated++) {
				let filePath = dirPath + "/random" + fileCreated + ".json";
				fs.writeFile(filePath, { [fileCreated]: "Hello" }, (err) => {
					fsRun += 1;
					if (err) {
						errLog.push("File creation failedd for: " + filePath);
					} else {
						fileCreatedPaths.push(filePath);
					}

					if (fileCreatedPaths.length === fileCount && fsRun === fileCount) {
						for (
							let fileDeleted = 0;
							fileDeleted < fileCreatedPaths.length;
							fileDeleted++
						) {
							setTimeout(() => {
								fsRun = 0;
								fs.unlink(fileCreatedPaths[fileDeleted], (err) => {
									fsRun += 1;
									if (err) {
										errLog.push(
											"Error while deleting" + fileCreatedPaths[fileDeleted]
										);
									} else {
										deleteCount += 1;
									}
									if (fsRun === fileCount) {
										if (errLog.length === 0) {
											cb(null, "Success");
										} else {
											cb(errLog);
										}
									}
								});
							}, 40 * 1000);
						}
					} else {
						if (errLog.length !== 0 && fsRun === fileCount) {
							cb(errLog);
						}
					}
				});
			}
		}
	});
}
module.exports = problem1;
