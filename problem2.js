/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/
const fs = require("fs");
const path = require("path");
function problem2(cb) {
	let errLog = [];
	let successLog = [];
	let deleteCount = 0;
	let fsRun = 0;
	fs.readFile("./lipsum.txt", "utf-8", (err, data) => {
		if (err) {
			errLog.push(err.message);
		} else {
			successLog.push("Completed reading the Lipsum.txt file");
			let lipsumData = data.toString().toUpperCase();
			let newFileName = "lipsumUpperCased.txt"; //1
			fs.writeFile(newFileName, lipsumData, (err) => {
				if (err) {
					errLog.push(err.message);
				} else {
					successLog.push("Successfully written into LipsumUpperCase.txt");
					fs.writeFile("filenames.txt", newFileName, (err) => {
						if (err) {
							errLog.push(err.message);
						} else {
							successLog.push("Filename added to filenames.txt");
							fs.readFile(newFileName, "utf-8", (err, data) => {
								if (err) {
									errLog.push(err.message);
								} else {
									let newFileNameContent = data.toString().toLowerCase();
									let sentencedNewFile = newFileNameContent
										.split(". ")
										.join("\n");
									let sentenceFileName = "sentencedLipsum.txt"; //2
									fs.writeFile(sentenceFileName, sentencedNewFile, (err) => {
										if (err) {
											errLog.push(err.message);
										} else {
											successLog.push(
												"Successfully written into sentencedLipsum.txt"
											);
											fs.appendFile(
												"filenames.txt",
												"\n" + sentenceFileName,
												(err) => {
													if (err) {
														errLog.push(err.message);
													} else {
														fs.readFile(
															sentenceFileName,
															"utf-8",
															(err, data) => {
																if (err) {
																	errLog.push(err.message);
																} else {
																	let sentenedDataArr = data.split("\n");
																	sentenedDataArr.sort(
																		(firstWord, secondWord) => {
																			return firstWord.localeCompare(
																				secondWord
																			);
																		}
																	);
																	let sentencedData = sentenedDataArr.join(" ");
																	let sortedFileName = "sortedFileName.txt"; //3
																	fs.writeFile(
																		sortedFileName,
																		sentencedData,
																		(err) => {
																			if (err) {
																				errLog.push(err.message);
																			}
																			successLog.push(
																				"Succesfully written into sortedFileName.txt"
																			);
																			fs.appendFile(
																				"filenames.txt",
																				"\n" + sortedFileName,
																				(err) => {
																					if (err) {
																						errLog.push(err.message);
																					} else {
																						successLog.push(
																							"Added sortedFileName.txt into filenames.txt"
																						);
																						fs.readFile(
																							"filenames.txt",
																							"utf-8",
																							(err, data) => {
																								if (err) {
																									errLog.push(err.message);
																								} else {
																									let fileNames =
																										data.split("\n");
																									setTimeout(() => {
																										for (
																											let index = 0;
																											index < fileNames.length;
																											index++
																										) {
																											fs.unlink(
																												fileNames[index],
																												(err) => {
																													fsRun += 1;
																													if (err) {
																														errLog.push(
																															err.message
																														);
																													} else {
																														deleteCount += 1;
																														successLog.push(
																															"Successfully : " +
																																fileNames[index]
																														);
																														if (
																															fsRun === 3 &&
																															deleteCount === 3
																														) {
																															if (
																																errLog.length ===
																																0
																															) {
																																cb(
																																	null,
																																	successLog
																																);
																															} else {
																																cb(
																																	errLog,
																																	successLog
																																);
																															}
																														}
																													}
																												}
																											);
																										}
																									}, 6000);
																								}
																							}
																						);
																					}
																				}
																			);
																		}
																	);
																}
															}
														);
													}
												}
											);
										}
									});
								}
							});
						}
					});
				}
			});
		}
	});
}
module.exports = problem2;
